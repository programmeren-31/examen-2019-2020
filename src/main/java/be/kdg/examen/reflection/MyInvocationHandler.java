package be.kdg.examen.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;

public class MyInvocationHandler implements InvocationHandler {

    private final List<ExpectedAction> actions;
    private int counter = 0;

    public MyInvocationHandler(List<ExpectedAction> actions) {
        this.actions = actions;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (args != null && args.length > 1) throw new WrongActionException("Te veel parameters");
        ExpectedAction expectedAction = actions.get(counter);

        if (!expectedAction.getMethodName().equals(method.getName())) throw new WrongActionException("Onjuiste method");
        if (args != null && args.length == 1) {
            System.out.println(expectedAction.getParameterValue());

            System.out.println(args[0]);
            if (!expectedAction.getParameterValue().equals(args[0].toString())) throw new WrongActionException("Parameter waarde onjuist");
        }
        counter++;
        System.out.println(expectedAction.getReturnValue());
        if (!expectedAction.getReturnValue().equalsIgnoreCase("null")) {
            if (int.class.equals(method.getReturnType())) {
                return Integer.parseInt(expectedAction.getReturnValue());
            } else if (char.class.equals(method.getReturnType())) {
                return expectedAction.getReturnValue().charAt(0);
            } else {
                return expectedAction.getReturnValue();
            }

        }



        return null;
    }
}
