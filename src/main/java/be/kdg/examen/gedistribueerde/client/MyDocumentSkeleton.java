package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class MyDocumentSkeleton implements Runnable {
    private final MessageManager messageManager;
    private final MyDocument document;

    public MyDocumentSkeleton(MyDocumentImpl myDocument) {
        this.messageManager = new MessageManager();
        this.document = myDocument;
    }

    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    @Override
    public void run() {
        Thread thread = new Thread(this::runInThread);
        thread.start();
    }

    private void runInThread() {
        while(true) {
            MethodCallMessage methodCallMessage = messageManager.wReceive();
            handleRequest(methodCallMessage);
        }
    }

    private void handleRequest(MethodCallMessage methodCallMessage) {
        if ("getText".equals(methodCallMessage.getMethodName())) {
            MethodCallMessage message = new MethodCallMessage(getAddress(), "answer");
            message.setParameter("result", document.getText());
            messageManager.send(message, methodCallMessage.getOriginator());
        } else if ("setText".equals(methodCallMessage.getMethodName())) {
            document.setText(methodCallMessage.getParameter("text"));
            messageManager.send(new MethodCallMessage(getAddress(), "ok"), methodCallMessage.getOriginator());
        }
    }
}
