package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

public class StartClient {
    public static void main(String[] args) {
        String serverIp = "10.140.96.113";
        int serverPort = 65257;

        NetworkAddress serverAddress = new NetworkAddress(serverIp, serverPort);

        MyDocumentImpl document = new MyDocumentImpl();
        MyDocumentSkeleton documentSkeleton = new MyDocumentSkeleton(document);
        new Thread(documentSkeleton).start();

        Server serverStub = new ServerStub(serverAddress, documentSkeleton.getAddress());
        Client client = new Client(serverStub, document);
        client.run();
    }
}
