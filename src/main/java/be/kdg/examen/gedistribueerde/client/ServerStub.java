package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

public class ServerStub implements Server {
    private final MessageManager messageManager;
    private final NetworkAddress originator;
    private final NetworkAddress destination;

    public ServerStub(NetworkAddress anAddress, NetworkAddress anotherAddress) {
        messageManager = new MessageManager();
        originator = anotherAddress;
        destination = anAddress;
    }

    @Override
    public void convert(MyDocument document) {
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "convert");
        methodCallMessage.setParameter("destination", originator.toString());
        methodCallMessage.setParameter("test", document.getText());
        messageManager.send(methodCallMessage, destination);
        System.out.println("Send message");

        messageManager.wReceive();
    }
}
