package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class ServerSkeleton {
    private final MessageManager messageManager;
    private final Server server;
    public ServerSkeleton(Server server) {
        messageManager = new MessageManager();
        this.server = server;
    }

    public void run() {
        Thread thread = new Thread(this::runInThread);
        thread.start();
    }

    private void runInThread() {
        while (true) {
            MethodCallMessage methodCallMessage = messageManager.wReceive();
            handleRequest(methodCallMessage);
        }
    }

    private void handleRequest(MethodCallMessage methodCallMessage) {
        System.out.print("Got message");
        if (methodCallMessage.getMethodName().equals("convert")) {
            String address = methodCallMessage.getParameter("destination");
            NetworkAddress networkAddress = new NetworkAddress(address.split(":")[0],Integer.parseInt(address.split(":")[1]) );
            server.convert(new MyDocumentStub(networkAddress));
        }

        messageManager.send(new MethodCallMessage(getAddress(), "Ok"), methodCallMessage.getOriginator());
    }

    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }
}
