package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.MyDocument;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class MyDocumentStub implements MyDocument {
    private final MessageManager messageManager;
    private final NetworkAddress destination;
    public MyDocumentStub(NetworkAddress anAddress) {
        this.destination = anAddress;
        messageManager = new MessageManager();
    }

    @Override
    public String getText() {
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "getText");
        messageManager.send(methodCallMessage, destination);
        System.out.println("Waiting for message");
        MethodCallMessage message = messageManager.wReceive();
        System.out.println("Message received");
        return message.getParameter("result");
    }

    @Override
    public void setText(String text) {
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "setText");
        methodCallMessage.setParameter("text", text);
        messageManager.send(methodCallMessage, destination);
        System.out.println("Send message to set text");
        MethodCallMessage methodCallMessage1 = messageManager.wReceive();

    }
}
