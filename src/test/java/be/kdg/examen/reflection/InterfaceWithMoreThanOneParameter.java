package be.kdg.examen.reflection;

public interface InterfaceWithMoreThanOneParameter {
    void moreThanOneParam(String a, int b);
}
