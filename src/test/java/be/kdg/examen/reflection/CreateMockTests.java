package be.kdg.examen.reflection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CreateMockTests {

    List<ExpectedAction> actiondList = ExpectedActionsReader.readActionsFromFile("src/main/resources/MyScenario.txt");

    @Test
    void testCreateMock() {
        MyInterface myInterface = (MyInterface) MockFactory.create(MyInterface.class, actiondList);
        assertNotNull(myInterface);
    }

    @Test
    void testExceptionMoreThanOneParam() {
        InterfaceWithMoreThanOneParameter myInterface = (InterfaceWithMoreThanOneParameter) MockFactory.create(InterfaceWithMoreThanOneParameter.class, actiondList);
        try {
            myInterface.moreThanOneParam("", 1);
            fail();
        } catch (WrongActionException e) {
            //
        }


    }

    @Test
    void testExceptionWrongMethodName() {
        MyInterface myInterface1 = (MyInterface) MockFactory.create(MyInterface.class, actiondList);

        try {
            myInterface1.doeC();
            fail();
        } catch (WrongActionException e) {
            //
        }
    }

    @Test
    void testExceptionWrongParameterValue() {
        MyInterface myInterface1 = (MyInterface) MockFactory.create(MyInterface.class, actiondList);
        try {
            myInterface1.doeA(200);
            fail();
        } catch (WrongActionException e) {
            //
        }
    }

    @Test
    void testVolgordeVoid() {
        MyInterface myInterface = (MyInterface) MockFactory.create(MyInterface.class, actiondList);

        myInterface.doeA(5);
        myInterface.doeA(42);
    }

    @Test
    void testVolgordeReturn() {
        MyInterface myInterface = (MyInterface) MockFactory.create(MyInterface.class, actiondList);
        myInterface.doeA(5);
        myInterface.doeA(42);
        int a = myInterface.getA();
        assertEquals(a, 5);
    }

    @Test
    void testVolgordeReturn2() {
        MyInterface myInterface = (MyInterface) MockFactory.create(MyInterface.class, actiondList);
        myInterface.doeA(5);
        myInterface.doeA(42);
        int a = myInterface.getA();
        myInterface.doeB("Bla");
        myInterface.doeC();
        char c = myInterface.getC();
        assertEquals(c, 'A');
    }

    @Test
    void testVerkeerdeVolgord() {
        MyInterface myInterface = (MyInterface) MockFactory.create(MyInterface.class, actiondList);
        myInterface.doeA(5);

        try {
            myInterface.doeB("test");
            fail();
        } catch (WrongActionException e) {
            //
        }
    }

    @Test
    void testVolgordeReturnString() {
        MyInterface myInterface = (MyInterface) MockFactory.create(MyInterface.class, actiondList);
        myInterface.doeA(5);
        myInterface.doeA(42);
        int a = myInterface.getA();
        myInterface.doeB("Bla");
        myInterface.doeC();
        char c = myInterface.getC();
        assertEquals(c, 'A');
        String b = myInterface.getB(5);
        assertEquals(b, "Foo");
    }

}
